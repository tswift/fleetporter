package org.tswift.transfer.console.commands.loaders;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.SettingsAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CharacterDataAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SpecialItemData;
import com.fs.starfarer.api.campaign.SpecialItemSpecAPI;
import com.fs.starfarer.api.campaign.econ.CommoditySpecAPI;
import com.fs.starfarer.api.campaign.econ.EconomyAPI;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.characters.FullName.Gender;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.loading.HullModSpecAPI;
import com.fs.starfarer.api.loading.VariantSource;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.plugins.LevelupPlugin;

import org.tswift.transfer.console.commands.parsers.types.Cargo;
import org.tswift.transfer.console.commands.parsers.types.Commodity;
import org.tswift.transfer.console.commands.parsers.types.Fighter;
import org.tswift.transfer.console.commands.parsers.types.Fleet;
import org.tswift.transfer.console.commands.parsers.types.Officer;
import org.tswift.transfer.console.commands.parsers.types.Player;
import org.tswift.transfer.console.commands.parsers.types.Relation;
import org.tswift.transfer.console.commands.parsers.types.Ship;
import org.tswift.transfer.console.commands.parsers.types.Skill;
import org.tswift.transfer.console.commands.parsers.types.SpecialCommodity;
import org.tswift.transfer.console.commands.parsers.types.Stats;
import org.tswift.transfer.console.commands.parsers.types.Variant;

/**
 * PlayerLoader
 */
public class PlayerLoader {
    boolean has_nexerelin;
    CampaignFleetAPI fleet;
    CargoAPI cargo;
    FleetDataAPI fleet_data;
    MutableCharacterStatsAPI stats;
    CharacterDataAPI character;
    LevelupPlugin level_plugin;
    SettingsAPI settings;
    SectorAPI sector;

    public PlayerLoader() {
        has_nexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        fleet = Global.getSector().getPlayerFleet();
        fleet_data = fleet.getFleetData();
        stats = fleet.getCommander().getStats();
        sector = Global.getSector();
        character = Global.getSector().getCharacterData();
        cargo = Global.getFactory().createCargo(true);
        level_plugin = Global.getSettings().getLevelupPlugin();
        settings = Global.getSettings();
    }

    public void loadPlayer(Player p) {
        loadStats(p.getStats());
        loadRelations(p.getRelations());
        loadHullMods(p.getMods());
        loadCargo(p.getCargo());
        loadFleet(p.getFleet());
        loadOfficers(p.getOfficers());

    }

    public void loadStats(Stats s) {
        int max_level = level_plugin.getMaxLevel();
        long max_xp = level_plugin.getXPForLevel(max_level);
        long xp_to_add = Math.max(s.getXp(), max_xp);
        stats.addXP(xp_to_add);
        stats.levelUpIfNeeded();
        stats.setPoints(s.getPoints());
        List<String> all_aptitudes = settings.getAptitudeIds();
        List<String> all_skills = settings.getSkillIds();



        for(Skill skill : s.getSkills()) {
            if(skill.isAptitude()) {
                if(!all_aptitudes.contains(skill.getId())){
                    System.out.printf("no aptitude %s\n", skill.getId());
                    continue;
                }
                    stats.setAptitudeLevel(skill.getType(), skill.getLevel());
            } else {
                if(!all_skills.contains(skill.getId())){
                    System.out.printf("no skill %s\n", skill.getId());
                    continue;
                }
                stats.setSkillLevel(skill.getId(), skill.getLevel());
            }
        }

    }
    public void loadRelations(List<Relation> relations) {
        for(Relation r : relations) {
            FactionAPI faction = sector.getFaction(r.getFaction_id());
            if(faction == null) {
                System.out.printf("no faction %s\n", r.getFaction_id());
                continue;
            }
            faction.setRelationship(Factions.PLAYER, r.getRelationship());
        }
    }

    public void loadHullMods(List<String> hullmods) {
        for(String mod : hullmods) {
            HullModSpecAPI spec = Global.getSettings().getHullModSpec(mod);
            if(spec == null) {
                System.out.printf("no hullmod spec %s\n", mod);
                continue;
            }
            character.addHullMod(mod);
        }
    }

    public void loadCargo(Cargo cargo) {
        CargoAPI fleet_cargo = fleet.getCargo();
        EconomyAPI economy = sector.getEconomy();
        for(Commodity c: cargo.getCommodities()) {
            CommoditySpecAPI spec = economy.getCommoditySpec(c.getId());
            if(spec == null) {
                System.out.printf("no commodity spec for %s\n", c);
                continue;
            }
            fleet_cargo.addCommodity(c.getId(), c.getSize());
        }
        for(Integer c: cargo.getCrew()) {
            fleet_cargo.addCrew(c);
        }
        for(Integer c: cargo.getMarines()) {
            fleet_cargo.addMarines(c);
        }

        for(Commodity c: cargo.getWings()) {
            FighterWingSpecAPI spec = settings.getFighterWingSpec(c.getId());
            if (spec == null) {
                System.out.printf("no fighter spec %s\n", c);
                continue;
            }
            fleet_cargo.addFighters(c.getId(), (int)c.getSize());
        }
        for(Commodity c: cargo.getWeapons()) {
            WeaponSpecAPI spec = settings.getWeaponSpec(c.getId());
            if (spec == null) {
                System.out.printf("no weapon spec %s\n", c.getId());
                continue;
            }
            fleet_cargo.addWeapons(c.getId(), (int)c.getSize());
        }
        for(SpecialCommodity c: cargo.getSpecial()) {
            SpecialItemSpecAPI spec = settings.getSpecialItemSpec(c.getSpec_id());
            if (spec == null) {
                System.out.printf("no special spec %s\n", c.getSpec_id());
                continue;
            }
            SpecialItemData sp = new SpecialItemData(c.getSpec_id(), c.getData());
            fleet_cargo.addSpecial(sp, c.getSize());

        }
    }
    public void loadFleet(Fleet fleet) {
        int max_ships = settings.getMaxShipsInFleet();
        for(Fighter f : fleet.getFighters()) {
            if(fleet_data.getNumMembers() >= max_ships) {
                System.out.printf("figher over limit? %s\n", f);
            }
            FighterWingSpecAPI spec = settings.getFighterWingSpec(f.getSpec_id());
            if (spec == null) {
                System.out.printf("no fighter spec %s\n", f.getSpec_id());
                continue;
            }
            FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.FIGHTER_WING, f.getSpec_id());
            fleet_data.addFleetMember(member);
        }
        for(Ship s: fleet.getShips()) {
            if(fleet_data.getNumMembers() >= max_ships) {
                System.out.printf("ship over limit?\n", s);
            }
            ShipHullSpecAPI hull_spec = null;
            try {
                hull_spec = settings.getHullSpec(s.getHull_id());
            } catch (Exception ex) {}
            if (hull_spec == null) {
                System.out.printf("no hull spec %s\n", s.getHull_id());
                continue;
            }

            ShipVariantAPI  variant = loadVariant(s.getVariant());
            if (variant == null) {
                System.out.printf("no variant %s\n", s.getVariant().getClass());
                continue;
            }

            FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, variant);
            member.setShipName(s.getName());
            member.getRepairTracker().setCR(s.getCr());
            member.getRepairTracker().performRepairsFraction(s.getFraction());
            member.getStatus().setHullFraction(s.getFraction());
            fleet_data.addFleetMember(member);
        }
    }
    private ShipVariantAPI loadVariant(Variant v) {
        ShipVariantAPI game_variant = null;
        try {
            game_variant = settings.getVariant(v.getId());
        } catch (Exception ex) {}

        if(game_variant == null) {
            ShipHullSpecAPI hull_spec = settings.getHullSpec(v.getId());
            if(hull_spec == null) {
                return null;
            }
            game_variant = settings.createEmptyVariant(v.getName(), hull_spec);
            game_variant.setVariantDisplayName(v.getName());
        } else {
            game_variant = game_variant.clone();
        }
        game_variant.setNumFluxVents(v.getVents());
        game_variant.setNumFluxCapacitors(v.getCapacitors());
        game_variant.setSource(VariantSource.REFIT);
        for(String slot_id :  v.getWeapons().keySet()) {
            String weapon_id = v.getWeapons().get(slot_id);
            WeaponSpecAPI spec = settings.getWeaponSpec(weapon_id);
            if( spec == null) {
                System.out.printf("no weapon spec for %s\n", weapon_id);
                continue;
            }
            game_variant.addWeapon(slot_id, weapon_id);
        }
        Map<String, Variant> modules = v.getModules();
        for(String slot_id :  modules.keySet()) {
            Variant mv = modules.get(slot_id);
            ShipVariantAPI module_variant = settings.getVariant(v.getId());
            if(module_variant == null) {
                System.out.printf("no module variant for for %s\n", mv);
            }
            game_variant.setModuleVariant(slot_id, module_variant);
        }
        return game_variant;
    }
    public void loadOfficers(List<Officer> officers) {
        
        List<String> skill_ids = settings.getSkillIds();
        for(Officer o : officers) {
            SpriteAPI portrait = Global.getSettings().getSprite(o.getPortrait());
            if(portrait == null) {
                System.out.printf("no portrait for %s\n", o);
            }
            List<Skill> valid_skills = new ArrayList<>();
            for(Skill s: o.getSkills()) {
                if (!skill_ids.contains(s.getId())) {
                    System.out.printf("no skill %s\n", s);
                    continue;
                }
                valid_skills.add(s);
            }

            PersonAPI officer = sector.getFaction(Factions.PLAYER).createRandomPerson();
            OfficerDataAPI officer_data = Global.getFactory().createOfficerData(officer);
            for(Skill s: valid_skills) {
                for (int i = 0; i < s.getLevel(); i++) {
                    officer.getStats().increaseSkill(s.getId());
                }
            }
            officer.getName().setFirst(o.getFirst_name());
            officer.getName().setLast(o.getLast_name());
            try {
                Gender g = Gender.valueOf(o.getGender());
                officer.getName().setGender(g);
            } catch (Exception ex) {
                System.out.printf("no gender for %s\n", o);
                continue;
            }
            officer_data.addXP(o.getXp());
            officer.setPortraitSprite(o.getPortrait());
            officer.setRankId(o.getRank());
            officer.setPostId(o.getPost());
            officer.setPersonality(o.getPersonality());
            fleet.getFleetData().addOfficer(officer);
        }
    }
}
