package org.tswift.transfer.console.commands;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.fleet.FleetMemberAPI;

import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;
import org.tswift.transfer.console.commands.loaders.PlayerLoader;
import org.tswift.transfer.console.commands.parsers.types.Player;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class LoadSave implements BaseCommand {


    private static Iterator<String> dataToIterator(String data) {
        ArrayList<String> adata = new ArrayList<>();
        Collections.addAll(adata, data.split(";"));
        return adata.iterator();
    }

    private static Player deserializePlayerData(Iterator<String> dataiter) {
        try {
        return Player.deserialize(dataiter);
        } catch (Exception ex) {
            return null;
        }
    }

    private static boolean loadData(Player p) {
        PlayerLoader loader = new PlayerLoader();
        loader.loadPlayer(p);
        return true;
    }

    @Override
    public CommandResult runCommand(String args, CommandContext context) {
        boolean nexerelinExists = Global.getSettings().getModManager().isModEnabled("nexerelin");

        if (context != CommandContext.CAMPAIGN_MAP) {
            Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
            return CommandResult.WRONG_CONTEXT;
        }

        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable trans = clpbrd.getContents(this);
        String data;
        try {
            data = (String) trans.getTransferData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException | IOException ex) {
            Console.showMessage("Invalid clipboard data!");
            return CommandResult.ERROR;
        }

        String[] argList = args.split("\\s");
        Set<String> filteredArgs = new HashSet<>(argList.length);
        Iterator<String> iterator = dataToIterator(data);
        Player p = deserializePlayerData(iterator);
        if(p == null) {
            Console.showMessage("Trouble parsing");
            return CommandResult.ERROR;
        }
        if(!loadData(p)) {
            Console.showMessage("Trouble loading");
            return CommandResult.ERROR;
        }
        Console.showMessage("Good evening professor.");
        Console.showMessage("I see you hae driven here in your Ferrari.");
        return CommandResult.SUCCESS;
    }
}
