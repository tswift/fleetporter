package org.tswift.transfer.console.commands;

import com.fs.starfarer.api.Global;

import org.lazywizard.console.BaseCommand;
import org.lazywizard.console.CommonStrings;
import org.lazywizard.console.Console;
import org.tswift.transfer.console.commands.parsers.PlayerParser;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class CopySave implements BaseCommand {


    @SuppressWarnings("unchecked")
    private static String serializePlayerData() {
        boolean nexerelinExists = Global.getSettings().getModManager().isModEnabled("nexerelin");
        PlayerParser player = new PlayerParser();
        player.loadData();
        return player.toString();
    }

    @Override
    public CommandResult runCommand(String args, CommandContext context) {
        if (context != CommandContext.CAMPAIGN_MAP) {
            Console.showMessage(CommonStrings.ERROR_CAMPAIGN_ONLY);
            return CommandResult.WRONG_CONTEXT;
        }

        String data = serializePlayerData();

        StringSelection stringSelection = new StringSelection(data);
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);

        Console.showMessage("Save data copied to the clipboard.");
        return CommandResult.SUCCESS;
    }
}
