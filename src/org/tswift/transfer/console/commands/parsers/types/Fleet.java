package org.tswift.transfer.console.commands.parsers.types;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Fleet
 */
public class Fleet {
    List<Fighter> fighters;
    List<Ship> ships;

    public Fleet(List<Fighter> fighters, List<Ship> ships) {
        this.fighters = fighters;
        this.ships = ships;
    }

    public void serializeAppend(List<String> data) {
        data.add(String.valueOf(fighters.size()));
        for(Fighter f: fighters) {
            f.serializeAppend(data);
        }
        data.add(String.valueOf(ships.size()));
        for(Ship s: ships) {
            s.serializeAppend(data);
        }
    }
    public static Fleet deserialize(Iterator<String> data) {
        List<Fighter> fighters = new ArrayList<>();
        List<Ship> ships = new ArrayList<>();
        int fighters_count = Integer.parseInt(data.next());
        for (int i = 0; i < fighters_count; i++) {
            fighters.add(Fighter.deserialize(data));
        }
        int ship_count = Integer.parseInt(data.next());
        for (int i = 0; i < ship_count; i++) {
            ships.add(Ship.deserialize(data));
        }

        return new Fleet(
            fighters,
            ships
        );
    }

    @Override
    public String toString() {
        return "Fleet [fighters=" + fighters + ", ships=" + ships + "]";
    }

    public List<Fighter> getFighters() {
        return fighters;
    }

    public void setFighters(List<Fighter> fighters) {
        this.fighters = fighters;
    }

    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }
}
