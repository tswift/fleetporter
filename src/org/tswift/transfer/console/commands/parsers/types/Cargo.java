package org.tswift.transfer.console.commands.parsers.types;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Cargo
 */
public class Cargo {
    List<Commodity> commodities;
    List<Integer> marines;
    List<Integer> crew;
    List<Commodity> weapons;
    List<Commodity> wings;
    List<SpecialCommodity> special;
    float credits;
    public Cargo(List<Commodity> commodities, List<Integer> marines, List<Integer> crew, List<Commodity> weapons,
            List<Commodity> wings, List<SpecialCommodity> special, float credits) {
        this.commodities = commodities;
        this.marines = marines;
        this.crew = crew;
        this.weapons = weapons;
        this.wings = wings;
        this.special = special;
        this.credits = credits;
    }

    public static Cargo deserialize(Iterator<String> data) {
        List<Commodity> commodities = new ArrayList<>();
        List<Integer> marines = new ArrayList<>();
        List<Integer> crew = new ArrayList<>();
        List<Commodity> weapons = new ArrayList<>();
        List<Commodity> wings = new ArrayList<>();
        List<SpecialCommodity> special = new ArrayList<>();
        int commodities_count = Integer.parseInt(data.next());
        for (int i = 0; i < commodities_count; i++) {
            commodities.add(Commodity.deserialize(data));
        }
        int marines_count = Integer.parseInt(data.next());
        for (int i = 0; i < marines_count; i++) {
            marines.add(Integer.parseInt(data.next()));
        }
        int crew_count = Integer.parseInt(data.next());
        for (int i = 0; i < crew_count; i++) {
            marines.add(Integer.parseInt(data.next()));
        }
        int weapons_count = Integer.parseInt(data.next());
        for (int i = 0; i < weapons_count; i++) {
            weapons.add(Commodity.deserialize(data));
        }
        int wings_count = Integer.parseInt(data.next());
        for (int i = 0; i < wings_count; i++) {
            wings.add(Commodity.deserialize(data));
        }
        int special_count = Integer.parseInt(data.next());
        for (int i = 0; i < special_count; i++) {
            special.add(SpecialCommodity.deserialize(data));
        }

        float credits = Float.parseFloat(data.next());

        return new Cargo(
            commodities,
            marines,
            crew,
            weapons,
            wings,
            special,
            credits
        );
    }


    public void serializeAppend(List<String> data) {
        data.add(String.valueOf(commodities.size()));
        for(Commodity c: commodities) {
            c.serializeAppend(data);
        }
        data.add(String.valueOf(marines.size()));
        for(Integer c: marines) {
            data.add(String.valueOf(c));
        }

        data.add(String.valueOf(crew.size()));
        for(Integer c: crew) {
            data.add(String.valueOf(c));
        }

        data.add(String.valueOf(weapons.size()));
        for(Commodity c: weapons) {
            c.serializeAppend(data);
        }

        data.add(String.valueOf(wings.size()));
        for(Commodity c: wings) {
            c.serializeAppend(data);
        }

        data.add(String.valueOf(special.size()));
        for(SpecialCommodity c: special) {
            c.serializeAppend(data);
        }
        data.add(String.valueOf(credits));
    }

    @Override
    public String toString() {
        return "Cargo [commodities=" + commodities + ", credits=" + credits + ", crew=" + crew + ", marines=" + marines
                + ", special=" + special + ", weapons=" + weapons + ", wings=" + wings + "]";
    }

    public List<Commodity> getCommodities() {
        return commodities;
    }

    public void setCommodities(List<Commodity> commodities) {
        this.commodities = commodities;
    }

    public List<Integer> getMarines() {
        return marines;
    }

    public void setMarines(List<Integer> marines) {
        this.marines = marines;
    }

    public List<Integer> getCrew() {
        return crew;
    }

    public void setCrew(List<Integer> crew) {
        this.crew = crew;
    }

    public List<Commodity> getWeapons() {
        return weapons;
    }

    public void setWeapons(List<Commodity> weapons) {
        this.weapons = weapons;
    }

    public List<Commodity> getWings() {
        return wings;
    }

    public void setWings(List<Commodity> wings) {
        this.wings = wings;
    }

    public List<SpecialCommodity> getSpecial() {
        return special;
    }

    public void setSpecial(List<SpecialCommodity> special) {
        this.special = special;
    }

    public float getCredits() {
        return credits;
    }

    public void setCredits(float credits) {
        this.credits = credits;
    }
}
