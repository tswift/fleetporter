package org.tswift.transfer.console.commands.parsers.types;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Officer
 */
public class Officer {

    String first_name;
    String last_name;
    String gender;
    String portrait;
    String rank;
    String post;
    String personality;
    long xp;
    List<Skill> skills;

    public Officer(String first_name, String last_name, String gender, String portrait, String rank, String post,
            String personality, long xp) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.gender = gender;
        this.portrait = portrait;
        this.rank = rank;
        this.post = post;
        this.personality = personality;
        this.xp = xp;
        this.skills = new ArrayList<Skill>();
    }
    public Officer(String first_name, String last_name, String gender, String portrait, String rank, String post,
            String personality, long xp, List<Skill> skills) {
        this(first_name, last_name, gender, portrait, rank, post, personality, xp);
        for (Skill skill : skills) {
            this.addSkill(skill.id, skill.level);
        }
    }


    public void addSkill(String id, float level) {
        this.skills.add(new Skill(id, level, "o"));
    }

    public static Officer deserialize(Iterator<String> data) {
        List<Skill> skills = new ArrayList<>();
        String first_name = data.next();
        String last_name = data.next();
        String gender = data.next();
        String portrait = data.next();
        String rank = data.next();
        String post = data.next();
        String personality = data.next();
        long xp = Long.parseLong(data.next());
        Integer skills_count = Integer.parseInt(data.next());
        for (int i = 0; i < skills_count; i++) {
            skills.add(Skill.deserialize(data));
        }
        return new Officer(
            first_name,
            last_name,
            gender,
            portrait,
            rank,
            post,
            personality,
            xp,
            skills
        );
    }

    public void serializeAppend(List<String> data) {
        data.add(first_name);
        data.add(last_name);
        data.add(gender);
        data.add(portrait);
        data.add(rank);
        data.add(post);
        data.add(personality);
        data.add(String.valueOf(xp));
        data.add(String.valueOf(skills.size()));
        for(Skill s: skills) {
            s.serializeAppend(data);
        }
    }
    @Override
    public String toString() {
        return "Officer [first_name=" + first_name + ", gender=" + gender + ", last_name=" + last_name
                + ", personality=" + personality + ", portrait=" + portrait + ", post=" + post + ", rank=" + rank
                + ", skills=" + skills + ", xp=" + xp + "]";
    }
    public String getFirst_name() {
        return first_name;
    }
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }
    public String getLast_name() {
        return last_name;
    }
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getPortrait() {
        return portrait;
    }
    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }
    public String getRank() {
        return rank;
    }
    public void setRank(String rank) {
        this.rank = rank;
    }
    public String getPost() {
        return post;
    }
    public void setPost(String post) {
        this.post = post;
    }
    public String getPersonality() {
        return personality;
    }
    public void setPersonality(String personality) {
        this.personality = personality;
    }
    public long getXp() {
        return xp;
    }
    public void setXp(long xp) {
        this.xp = xp;
    }
    public List<Skill> getSkills() {
        return skills;
    }
    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
