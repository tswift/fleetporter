package org.tswift.transfer.console.commands.parsers.types;

import java.util.Iterator;
import java.util.List;

/**
 * InnerRelation
 */
public class Relation {
    String faction_id;
    float relationship;
    public Relation(String faction_id, float relationship) {
        this.faction_id = faction_id;
        this.relationship = relationship;
    }

    public void serializeAppend(List<String> data) {
        data.add(faction_id);
        data.add(String.valueOf(relationship));
    }

    public static Relation deserialize(Iterator<String> data) {
        return new Relation(
            data.next(),
            Float.parseFloat(data.next())
        );
    }

    @Override
    public String toString() {
        return "Relation [faction_id=" + faction_id + ", relationship=" + relationship + "]";
    }

    public String getFaction_id() {
        return faction_id;
    }

    public void setFaction_id(String faction_id) {
        this.faction_id = faction_id;
    }

    public float getRelationship() {
        return relationship;
    }

    public void setRelationship(float relationship) {
        this.relationship = relationship;
    }
}
