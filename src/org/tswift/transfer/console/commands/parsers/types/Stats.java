package org.tswift.transfer.console.commands.parsers.types;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Stats
 */
public class Stats {
    long xp;
    float level;
    int points;
    List<Skill> skills;
    public Stats(long xp, float level, int points, List<Skill> skills) {
        this.xp = xp;
        this.level = level;
        this.points = points;
        this.skills = skills;
    }

    public void serializeAppend(List<String> data) {
        data.add(String.valueOf(xp));
        data.add(String.valueOf(level));
        data.add(String.valueOf(points));
        data.add(String.valueOf(skills.size()));
        for(Skill s: skills) {
            s.serializeAppend(data);
        }
    }
    public static Stats deserialize(Iterator<String> data) {
        List<Skill> skills = new ArrayList<>();
        long xp = Long.parseLong(data.next());
        float level = Float.parseFloat(data.next());
        int points = Integer.parseInt(data.next());
        int skill_count = Integer.parseInt(data.next());
        for (int i = 0; i < skill_count; i++) {
            skills.add(Skill.deserialize(data));
        }

        return new Stats(
            xp,
            level,
            points,
            skills
        );

    }

    @Override
    public String toString() {
        return "Stats [level=" + level + ", skills=" + skills + ", xp=" + xp + "]";
    }

    public long getXp() {
        return xp;
    }

    public void setXp(long xp) {
        this.xp = xp;
    }

    public float getLevel() {
        return level;
    }

    public void setLevel(float level) {
        this.level = level;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
