package org.tswift.transfer.console.commands.parsers.types;

import java.util.Iterator;
import java.util.List;

/**
 * SpecialCommodity
 */
public class SpecialCommodity {
    float size;
    String spec_id;
    String type;
    String data;
    public SpecialCommodity(float size, String spec_id, String type, String data) {
        this.size = size;
        this.spec_id = spec_id;
        this.type = type;
        this.data = data;
    }

    public void serializeAppend(List<String> data) {
        data.add(String.valueOf(size));
        data.add(spec_id);
        data.add(type);
        data.add(this.data);
    }

    public static SpecialCommodity deserialize(Iterator<String> data) {
        return new SpecialCommodity(
            Float.parseFloat(data.next()),
            data.next(),
            data.next(),
            data.next()
        );
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public String getSpec_id() {
        return spec_id;
    }

    public void setSpec_id(String spec_id) {
        this.spec_id = spec_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
