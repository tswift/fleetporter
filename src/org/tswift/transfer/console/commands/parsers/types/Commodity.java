package org.tswift.transfer.console.commands.parsers.types;

import java.util.Iterator;
import java.util.List;

/**
 * Commodity
 */
public class Commodity {
    String id;
    float size;
    String type;

    public Commodity(String id, float size, String type) {
        this.id = id;
        this.size = size;
        this.type = type;
    }

    public void serializeAppend(List<String> data) {
        data.add(id);
        data.add(String.valueOf(size));
        data.add(type);
    }
    public static Commodity deserialize(Iterator<String> data) {
        return new Commodity(
            data.next(),
            Float.parseFloat(data.next()),
            data.next()
        );
    }

    @Override
    public String toString() {
        return "Commodity [id=" + id + ", size=" + size + ", type=" + type + "]";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
