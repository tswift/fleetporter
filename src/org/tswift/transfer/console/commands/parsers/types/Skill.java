package org.tswift.transfer.console.commands.parsers.types;

import java.util.Iterator;
import java.util.List;

/**
 * Skill
 */
public class Skill {
    String id;
    float level;
    String type;

    public Skill(String id, float level, String type) {
        this.id = id;
        this.level = level;
        this.type = type;
    }
    public void serializeAppend(List<String> data) {
        data.add(id);
        data.add(String.valueOf(level));
        data.add(String.valueOf(type));
    }
    public static Skill deserialize(Iterator<String> data) {
        return new Skill(
            data.next(),
            Float.parseFloat(data.next()),
            data.next()
        );
    }
    @Override
    public String toString() {
        return "Skill [id=" + id + ", level=" + level + ", type=" + type + "]";
    }

    public boolean isAptitude() {
        return this.type == "a";
    }
    public boolean isSkill() {
        return this.type == "a";
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public float getLevel() {
        return level;
    }
    public void setLevel(float level) {
        this.level = level;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}

