package org.tswift.transfer.console.commands.parsers.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Variant
 */
public class Variant {
    String  name;
    String id;
    String spec_id;
    int vents;
    int capacitors;

    Map<String, String> weapons;
    List<String> wings;
    Map<String, Variant> modules;

    public Variant(String name, String id, String spec_id, int vents, int capacitors, Map<String, String> weapons, List<String> wings,
            Map<String, Variant> modules) {
        this.name = name;
        this.id = id;
        this.spec_id = spec_id;
        this.vents = vents;
        this.capacitors = capacitors;
        this.weapons = weapons;
        this.wings = wings;
        this.modules = modules;
    }

    public void serializeAppend(List<String> data) {
        data.add(name);
        data.add(id);
        data.add(spec_id);
        data.add(String.valueOf(vents));
        data.add(String.valueOf(capacitors));

        data.add(String.valueOf(weapons.size()));
        for(String w: weapons.keySet()) {
            data.add(w);
            data.add(weapons.get(w));
        }
        data.add(String.valueOf(wings.size()));
        for(String w: wings) {
            data.add(w);
        }
        data.add(String.valueOf(modules.size()));

        for(String w: modules.keySet()) {
            data.add(w);
            modules.get(w).serializeAppend(data);
        }
    }
    public static Variant deserialize(Iterator<String> data) {
        Map<String, String> weapons = new HashMap<>();
        List<String> wings = new ArrayList<>();
        Map<String, Variant> modules = new HashMap<>();
        String _name = data.next();
        String _id = data.next();
        String _spec_id = data.next();
        int _vents = Integer.parseInt(data.next());
        int _capacitors = Integer.parseInt(data.next());
        int weapons_count = Integer.parseInt(data.next());
        for (int i = 0; i < weapons_count; i++) {
            weapons.put(data.next(), data.next());
        }
        int wings_count = Integer.parseInt(data.next());
        for (int i = 0; i < wings_count; i++) {
            wings.add(data.next());
        }
        int modules_count = Integer.parseInt(data.next());
        for (int i = 0; i < modules_count; i++) {
            modules.put(data.next(), Variant.deserialize(data));
        }
        return new Variant(
            _name,
            _id,
            _spec_id,
            _vents,
            _capacitors,
            weapons,
            wings,
            modules
        );
    }

    @Override
    public String toString() {
        return "Variant [modules=" + modules + ", weapons=" + weapons + ", wings=" + wings + "]";
    }

    public Map<String, String> getWeapons() {
        return weapons;
    }

    public void setWeapons(Map<String, String> weapons) {
        this.weapons = weapons;
    }

    public List<String> getWings() {
        return wings;
    }

    public void setWings(List<String> wings) {
        this.wings = wings;
    }

    public Map<String, Variant> getModules() {
        return modules;
    }

    public void setModules(Map<String, Variant> modules) {
        this.modules = modules;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVents() {
        return vents;
    }

    public void setVents(int vents) {
        this.vents = vents;
    }

    public int getCapacitors() {
        return capacitors;
    }

    public void setCapacitors(int capacitors) {
        this.capacitors = capacitors;
    }

}
