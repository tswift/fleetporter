package org.tswift.transfer.console.commands.parsers.types;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Player
 */
public class Player {

    Stats stats;
    List<Relation> relations;
    List<String> mods;
    Cargo cargo;
    Fleet fleet;
    List<Officer> officers;

    public Player(Stats stats, List<Relation> relations, List<String> mods, Cargo cargo, Fleet fleet,
            List<Officer> officers) {
        this.stats = stats;
        this.relations = relations;
        this.mods = mods;
        this.cargo = cargo;
        this.fleet = fleet;
        this.officers = officers;
    }
    public String serialize() {
        List<String> data = new ArrayList<>();
        stats.serializeAppend(data);

        data.add(String.valueOf(relations.size()));
        for (Relation r : relations) {
            r.serializeAppend(data);
        }

        data.add(String.valueOf(mods.size()));
        for (String r : mods) {
            data.add(r);
        }

        cargo.serializeAppend(data);
        fleet.serializeAppend(data);
        data.add(String.valueOf(officers.size()));
        for (Officer o : officers) {
            o.serializeAppend(data);
        }
        StringBuilder sb = new StringBuilder();
        for (String s : data) {
            sb.append(s).append(";");
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    public static Player deserialize(Iterator<String> data) {
        List<Relation> relations = new ArrayList<>();
        List<String> mods = new ArrayList<>();
        List<Officer> officers = new ArrayList<>();

        Stats stats = Stats.deserialize(data);
        System.out.println(stats);

        Integer relation_count = Integer.parseInt(data.next());
        System.out.printf("%d relations :\n", relation_count);
        for (int i = 0; i < relation_count; i++) {
            Relation r = Relation.deserialize(data);
            System.out.println(r);
            relations.add(r);
        }

        Integer mod_count = Integer.parseInt(data.next());
        System.out.printf("%d mods:\n", mod_count);
        for (int i = 0; i < mod_count; i++) {
            String mod = data.next();
            System.out.println(mod);
            mods.add(mod);
        }

        Cargo cargo = Cargo.deserialize(data);
        System.out.println(cargo);
        
        Fleet fleet = Fleet.deserialize(data);
        System.out.println(fleet);
        Integer officer_count = Integer.parseInt(data.next());
        System.out.printf("%d officers:\n", officer_count);
        for (int i = 0; i < officer_count; i++) {
            Officer o = Officer.deserialize(data);
            System.out.println(o);
            officers.add(o);
        }
        Player p = new Player(
                stats,
                relations,
                mods,
                cargo,
                fleet,
                officers
        );
        System.out.println(p);
        return p;
    }
    @Override
    public String toString() {
        return "Player [cargo=" + cargo + ", fleet=" + fleet + ", mods=" + mods + ", officers=" + officers
                + ", relations=" + relations + ", stats=" + stats + "]";
    }
    public Stats getStats() {
        return stats;
    }
    public void setStats(Stats stats) {
        this.stats = stats;
    }
    public List<Relation> getRelations() {
        return relations;
    }
    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }
    public List<String> getMods() {
        return mods;
    }
    public void setMods(List<String> mods) {
        this.mods = mods;
    }
    public Cargo getCargo() {
        return cargo;
    }
    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }
    public Fleet getFleet() {
        return fleet;
    }
    public void setFleet(Fleet fleet) {
        this.fleet = fleet;
    }
    public List<Officer> getOfficers() {
        return officers;
    }
    public void setOfficers(List<Officer> officers) {
        this.officers = officers;
    }
}
