package org.tswift.transfer.console.commands.parsers.types;

import java.util.Iterator;
import java.util.List;

/**
 * Ship
 */
public class Ship {
    String hull_id;
    String name;
    float cr;
    float fraction;
    float repair;
    Variant variant;

    public Ship(String hull_id, String name, float cr, float fraction, float repair, Variant variant) {
        this.hull_id = hull_id;
        this.cr = cr;
        this.name = name;
        this.fraction = fraction;
        this.repair = repair;
        this.variant = variant;
    }

    public void serializeAppend(List<String> data) {
        data.add(hull_id);
        data.add(name);
        data.add(String.valueOf(cr));
        data.add(String.valueOf(fraction));
        data.add(String.valueOf(repair));
        variant.serializeAppend(data);
    }

    public static Ship deserialize(Iterator<String> data) {
        return new Ship(
            data.next(),
            data.next(),
            Float.parseFloat(data.next()),
            Float.parseFloat(data.next()),
            Float.parseFloat(data.next()),
            Variant.deserialize(data)
        );
    }

    public String getHull_id() {
        return hull_id;
    }

    public void setHull_id(String hull_id) {
        this.hull_id = hull_id;
    }

    public float getCr() {
        return cr;
    }

    public void setCr(float cr) {
        this.cr = cr;
    }

    public float getFraction() {
        return fraction;
    }

    public void setFraction(float fraction) {
        this.fraction = fraction;
    }

    public float getRepair() {
        return repair;
    }

    public void setRepair(float repair) {
        this.repair = repair;
    }

    public Variant getVariant() {
        return variant;
    }

    public void setVariant(Variant variant) {
        this.variant = variant;
    }

    @Override
    public String toString() {
        return "Ship [cr=" + cr + ", fraction=" + fraction + ", hull_id=" + hull_id + ", name=" + name + ", repair="
                + repair + ", variant=" + variant + "]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
