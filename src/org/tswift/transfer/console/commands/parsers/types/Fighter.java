package org.tswift.transfer.console.commands.parsers.types;

import java.util.Iterator;
import java.util.List;

/**
 * Fighter
 */
public class Fighter {

    String spec_id;
    float cr;

    public Fighter(String spec_id, float cr) {
        this.spec_id = spec_id;
        this.cr = cr;
    }

    public void serializeAppend(List<String> data) {
        data.add(spec_id);
        data.add(String.valueOf(cr));
    }

    public static Fighter deserialize(Iterator<String> data) {
        return new Fighter(
                data.next(),
                Float.parseFloat(data.next())
        );
    }

    @Override
    public String toString() {
        return "Fighter [cr=" + cr + ", spec_id=" + spec_id + "]";
    }

    public String getSpec_id() {
        return spec_id;
    }

    public void setSpec_id(String spec_id) {
        this.spec_id = spec_id;
    }

    public float getCr() {
        return cr;
    }

    public void setCr(float cr) {
        this.cr = cr;
    }
}
