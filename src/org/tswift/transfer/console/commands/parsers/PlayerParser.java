package org.tswift.transfer.console.commands.parsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.CharacterDataAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetDataAPI;
import com.fs.starfarer.api.campaign.SpecialItemData;
import com.fs.starfarer.api.campaign.SpecialItemSpecAPI;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.characters.SkillSpecAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.loading.FighterWingSpecAPI;
import com.fs.starfarer.api.loading.WeaponSpecAPI;

import org.tswift.transfer.console.commands.parsers.types.Cargo;
import org.tswift.transfer.console.commands.parsers.types.Commodity;
import org.tswift.transfer.console.commands.parsers.types.Fighter;
import org.tswift.transfer.console.commands.parsers.types.Fleet;
import org.tswift.transfer.console.commands.parsers.types.Officer;
import org.tswift.transfer.console.commands.parsers.types.Player;
import org.tswift.transfer.console.commands.parsers.types.Relation;
import org.tswift.transfer.console.commands.parsers.types.Ship;
import org.tswift.transfer.console.commands.parsers.types.Skill;
import org.tswift.transfer.console.commands.parsers.types.SpecialCommodity;
import org.tswift.transfer.console.commands.parsers.types.Stats;
import org.tswift.transfer.console.commands.parsers.types.Variant;

public class PlayerParser {
    boolean has_nexerelin;
    CampaignFleetAPI fleet;
    CargoAPI cargo;
    FleetDataAPI fleet_data;
    MutableCharacterStatsAPI stats;
    CharacterDataAPI character;

    public PlayerParser() {
    }

    public void loadData() {
        has_nexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        fleet = Global.getSector().getPlayerFleet();
        stats = fleet.getCommander().getStats();
        character = Global.getSector().getCharacterData();

        cargo = Global.getFactory().createCargo(true);
        for (CargoStackAPI stack : fleet.getCargo().getStacksCopy()) {
            cargo.addFromStack(stack);
        }
    }

    private Stats serializeStats() {

        List<Skill> skills = new ArrayList<>();

        for (String id : Global.getSettings().getSkillIds()) {
            SkillSpecAPI spec = Global.getSettings().getSkillSpec(id);

            List<String> aptitudes = Global.getSettings().getAptitudeIds();

            if (spec.isAptitudeEffect()) {
                String aptitude_id = id.replace("aptitude_", "");
                if (aptitudes.contains(aptitude_id) && stats.getAptitudeLevel(aptitude_id) > 0) {
                    skills.add(new Skill(id, stats.getAptitudeLevel(aptitude_id), "a"));
                }
            } else {
                    skills.add(new Skill(id, stats.getSkillLevel(id), "s"));
            }
        }
        return new Stats(
            stats.getXP(),
            stats.getLevel(),
            stats.getPoints(),
            skills
        );
    }

    private List<Relation> serializeRelations() {
        List<Relation> relations = new ArrayList<>();
        for (FactionAPI faction : Global.getSector().getAllFactions()) {
            if (faction.getId().equals(Factions.PLAYER) || faction.getId().equals(Factions.NEUTRAL)) {
                continue;
            }
            relations.add(new Relation(
                        faction.getId(),
                        faction.getRelationship(Factions.PLAYER)
            ));
        }
        return relations;
    }

    private List<String> serializeHullMods() {
        List<String> list = new ArrayList<>();
        for (String id : character.getHullMods()) {
            list.add(id);
        }
        return list;
    }


    private Variant serializeVariant(ShipVariantAPI v) {


            Map<String, String> weapons = new HashMap<>();
            List<String> wings = new ArrayList<>();
            Map<String, Variant> modules = new HashMap<>();
            for (String slot : v.getNonBuiltInWeaponSlots()) {
                weapons.put(slot, v.getWeaponId(slot));
            }

            for (String id : v.getNonBuiltInWings()) {
                wings.add(id);
            }

            for (String id : v.getStationModules().keySet()) {
                ShipVariantAPI mod_v = v.getModuleVariant(id);
                if (mod_v == null) { continue ; }
                modules.put(id, serializeVariant(mod_v));
            }

        return new Variant(
            v.getDisplayName(),
            v.getHullSpec().getHullId(),
            v.getHullVariantId(),
            v.getNumFluxVents(),
            v.getNumFluxCapacitors(),

            weapons,
            wings,
            modules
        );
    }
    private List<Officer> serializeOfficers() {
        List<Officer> officers = new ArrayList<>();
        for (OfficerDataAPI officerData : fleet.getFleetData().getOfficersCopy()) {
            PersonAPI officer = officerData.getPerson();
            Officer o = new Officer(
                officer.getName().getFirst(),
                officer.getName().getLast(),
                officer.getName().getGender().name(),
                officer.getPortraitSprite(),
                officer.getRankId(),
                officer.getPostId(),
                officer.getPersonalityAPI().getId(),
                officer.getStats().getXP()
            );
            for (String id : Global.getSettings().getSkillIds()) {
                if (officer.getStats().getSkillLevel(id) > 0) {
                    o.addSkill(id, officer.getStats().getSkillLevel(id));
                }
            }
            officers.add(o);
        }
        return officers;
    }

    private Cargo serializeCargo() {
        List<Commodity> commodities = new ArrayList<>();
        List<Integer> marines = new ArrayList<>();
        List<Integer> crew = new ArrayList<>();
        List<Commodity> weapons = new ArrayList<>();

        List<Commodity> wings = new ArrayList<>();
        List<SpecialCommodity> specials = new ArrayList<>();

        for (CargoStackAPI stack : cargo.getStacksCopy()) {
            if (stack.isCommodityStack()) {
                commodities.add(new Commodity(
                    stack.getCommodityId(),
                    stack.getSize(),
                    "c"
                ));
            }
            if (stack.isMarineStack()) {
                marines.add((int)stack.getSize());
            }
            if (stack.isCrewStack()) {
                crew.add((int)stack.getSize());
            }
            if (stack.isWeaponStack()) {
                WeaponSpecAPI wpn = stack.getWeaponSpecIfWeapon();
                weapons.add(new Commodity(wpn.getWeaponId(), stack.getSize(), "w"));
            }
            if (stack.isSpecialStack()) {
                SpecialItemSpecAPI special = stack.getSpecialItemSpecIfSpecial();
                SpecialItemData special_data = stack.getSpecialDataIfSpecial();
                specials.add(new SpecialCommodity(
                    stack.getSize(),
                    special.getId(),
                    special_data.getId(),
                    special_data.getData()
                ));
            }
            if (stack.isFighterWingStack()) {
                FighterWingSpecAPI wing = stack.getFighterWingSpecIfWing();
                wings.add(new Commodity(wing.getId(), stack.getSize(), "wing"));
            }
        }
        return new Cargo(
                commodities,
                marines,
                crew,
                weapons,
                wings,
                specials,
                cargo.getCredits().get()
        );
    }

    private Fleet serializeFleet() {
        List<Fighter> fighters = new ArrayList<>();
        List<Ship> ships = new ArrayList<>();
        List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
        for (FleetMemberAPI m : members)  {
            if(m.isFighterWing()) {
                fighters.add(new Fighter(
                            m.getSpecId(),
                            m.getRepairTracker().getBaseCR()
                ));
            }
            ShipVariantAPI variant = m.getVariant();


            for (String id : variant.getStationModules().keySet()) {
                ShipVariantAPI module = m.getVariant().getModuleVariant(id);
                if (module == null) { continue ; }
            }

            ships.add(new Ship(
                m.getHullId(),
                m.getShipName(),
                m.getRepairTracker().getBaseCR(),
                m.getStatus().getHullFraction(),
                m.getRepairTracker().computeRepairednessFraction(),
                serializeVariant(m.getVariant())
                ));
        }
        return new Fleet(fighters, ships);
    }

    public Player toType() {

        return new Player(
            serializeStats(),
            serializeRelations(),
            serializeHullMods(),
            serializeCargo(),
            serializeFleet(),
            serializeOfficers()
        );
    }

    @Override
    public String toString() {
        return toType().serialize();
    }
}
